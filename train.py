import argparse

import numpy as np
import torch

import src.losses.losses as module_loss
import src.models as module_arch
import src.logger.writers as module_writers
from src.datasets.util import get_dataloaders
from src.trainer.trainer import Trainer
from src.utils.parse_config import ConfigParser


SEED = 0
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)


def unpack_activations(arch_config):
    for key in arch_config["args"]:
        if key.endswith("_activation"):
            activation = config.init_obj(arch_config["args"][key], torch.nn)
            arch_config["args"][key] = activation
    return arch_config


def main(config):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    gen = config.init_obj(unpack_activations(config["gen_arch"]), module_arch)
    gen = gen.to(device)
    print("Generator")
    print(gen)

    gen_loss = config.init_obj(config["gen_loss"], module_loss).to(device)
    trainable_params = filter(lambda p: p.requires_grad, gen.parameters())
    gen_opt = config.init_obj(config["gen_opt"], torch.optim, trainable_params)

    disc = config.init_obj(unpack_activations(config["disc_arch"]), module_arch)
    disc = disc.to(device)
    print("Discriminator")
    print(disc)

    disc_loss = config.init_obj(config["disc_loss"], module_loss).to(device)
    trainable_params = filter(lambda p: p.requires_grad, disc.parameters())
    disc_opt = config.init_obj(config["disc_opt"], torch.optim, trainable_params)

    dataloaders = get_dataloaders(config)

    if config["trainer"]["visualize"] == "wandb":
        writer_manager = module_writers.WBWriter
    else:
        writer_manager = module_writers.PlaceholderWriter
    proj_name = config["trainer"]["wandb_project"]

    with writer_manager(proj_name, config) as writer:
        trainer = Trainer(
            gen,
            gen_loss,
            gen_opt,
            disc,
            disc_loss,
            disc_opt,
            device=device,
            train_dataloader=dataloaders["train"],
            val_dataloader=dataloaders["val"],
            writer=writer,
            len_epoch=config["trainer"].get("len_epoch", None),
            num_epochs=config["trainer"].get("epochs", None),
            disc_update_step=config["trainer"].get("disc_update_step", None),
            max_gradient_norm=config["trainer"].get("grad_norm_clip", 10)
        )

        trainer.train()


if __name__ == "__main__":
    args = argparse.ArgumentParser(description="PyTorch Template")
    args.add_argument(
        "-c",
        "--config",
        default=None,
        type=str,
        help="config file path (default: None)",
    )

    config = ConfigParser.from_args(args)
    main(config)
