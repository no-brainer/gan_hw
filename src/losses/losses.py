import torch
import torch.nn as nn
import torch.nn.functional as F


class ReconstructionLoss(nn.Module):

    def __init__(self):
        super(ReconstructionLoss, self).__init__()

    def forward(self, trg_imgs, pred_imgs, *args, **kwargs):
        return F.l1_loss(pred_imgs, trg_imgs)


class AdversarialLoss(nn.Module):

    def __init__(self):
        super(AdversarialLoss, self).__init__()

    def forward(self, fake_probs, *args, **kwargs):
        return F.binary_cross_entropy(fake_probs, torch.ones_like(fake_probs))


class DiscriminatorLoss(nn.Module):

    def __init__(self):
        super(DiscriminatorLoss, self).__init__()

    def forward(self, real_probs, fake_probs, *args, **kwargs):
        return 0.5 * (
            F.binary_cross_entropy(real_probs, torch.ones_like(real_probs)) +
            F.binary_cross_entropy(fake_probs, torch.zeros_like(fake_probs))
        )


class CombinationLoss(nn.Module):

    def __init__(self, alpha):
        super(CombinationLoss, self).__init__()
        self.alpha = alpha

        self.adv_loss = AdversarialLoss()
        self.rec_loss = ReconstructionLoss()

    def forward(self, *args, **kwargs):
        return self.adv_loss(*args, **kwargs) + self.alpha * self.rec_loss(*args, **kwargs)
