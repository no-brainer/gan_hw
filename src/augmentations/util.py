import torch
import torchvision

from src.utils.parse_config import ConfigParser


class AugmentationWrapper:

    def __init__(self, aug_fn):
        self.aug_fn = aug_fn

    def __call__(self, src_img, trg_img):
        img = torch.cat((src_img, trg_img), dim=0)
        out = self.aug_fn(img)
        src_img, trg_img = torch.split(out, 3, dim=0)
        return src_img, trg_img


def prepare_augmentations(config: ConfigParser):
    augs = []
    for aug_config in config["augmentations"]:
        aug = config.init_obj(aug_config, torchvision.transforms)
        augs.append(AugmentationWrapper(aug))
    return augs
