import contextlib
import os
import tarfile
from urllib import request
import zipfile

from torch.utils.data import DataLoader

from src.augmentations.util import prepare_augmentations
import src.datasets
from src.utils.parse_config import ConfigParser


def download_and_unpack(url, unpack_dir):
    os.makedirs(unpack_dir, exist_ok=True)

    filename = os.path.basename(url)
    local_filename = os.path.join(os.path.dirname(unpack_dir), filename)
    request.urlretrieve(url, local_filename)

    if local_filename.endswith(".zip"):
        with zipfile.ZipFile(local_filename, "r") as zip_file:
            zip_file.extractall(unpack_dir)

    elif local_filename.endswith(".tar.gz"):
        with contextlib.closing(tarfile.open(local_filename, "r")) as tar_file:
            tar_file.extractall(unpack_dir)

    else:
        raise RuntimeError(f"Unknown file extension: {filename}")

    os.remove(local_filename)


def get_dataloaders(configs: ConfigParser):
    dataloaders = {}
    for split, params in configs["data"].items():
        num_workers = params.get("num_workers", 1)

        augs = None
        if split == "train":
            augs = prepare_augmentations(configs)

        dataset = configs.init_obj(params["dataset"], src.datasets, augs=augs)

        if "batch_size" in params:
            bs = params["batch_size"]
            shuffle = True
        else:
            raise Exception()

        dataloader = DataLoader(dataset, batch_size=bs, shuffle=shuffle, num_workers=num_workers)
        dataloaders[split] = dataloader

    return dataloaders
