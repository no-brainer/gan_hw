import os

from PIL import Image
from torch.utils.data import Dataset
import torchvision
import torchvision.transforms.functional as F


from src.datasets.util import download_and_unpack


class AlignedImagesDataset(Dataset):

    def __init__(self, root, dataset_name, mode, limit=None, augs=None):
        dataset_path = os.path.join(root, dataset_name)
        if not os.path.exists(dataset_path):
            if dataset_name == "custom_maps":
                url = "https://filebin.net/0asmgfjmzh3z2jas/custom_maps.tar.gz"
            else:
                url = f"http://efrosgans.eecs.berkeley.edu/pix2pix/datasets/{dataset_name}.tar.gz"

            download_and_unpack(url, root)

        self.dataset_path = os.path.join(dataset_path, mode)
        self.size = len(os.listdir(self.dataset_path))
        self.limit = limit

        if augs is None:
            augs = []

        self.augs = augs

    def __len__(self):
        if self.limit is not None:
            return self.limit

        return self.size

    def __getitem__(self, idx):
        img = Image.open(os.path.join(self.dataset_path, f"{idx + 1}.jpg"))

        width, height = img.size
        mid = width // 2

        trg_img = F.to_tensor(img.crop((0, 0, mid, height)))
        src_img = F.to_tensor(img.crop((mid, 0, width, height)))

        for aug in self.augs:
            src_img, trg_img = aug(src_img, trg_img)

        src_img = F.normalize(src_img, (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        trg_img = F.normalize(trg_img, (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))

        return {
            "src_imgs": src_img,
            "trg_imgs": trg_img,
        }
