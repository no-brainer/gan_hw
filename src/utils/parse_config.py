import json
import os
from datetime import datetime
from functools import reduce, partial
from operator import getitem
from pathlib import Path

from src.utils import read_json, write_json, ROOT_PATH


class ConfigParser:
    def __init__(self, config, resume=None, modification=None, run_id=None):
        # load config file and apply modification
        self._config = _update_config(config, modification)
        self.resume = resume

        # set save_dir where trained model and log will be saved.
        save_dir = Path(self.config["trainer"]["save_dir"])

        exper_name = self.config["name"]
        if run_id is None:  # use timestamp as default run-id
            run_id = datetime.now().strftime(r"%m%d_%H%M%S%f")
        self._save_dir = save_dir / "models" / exper_name / run_id
        self._log_dir = save_dir / "log" / exper_name / run_id

        # make directory for saving checkpoints and log.
        exist_ok = run_id == ""
        self.save_dir.mkdir(parents=True, exist_ok=exist_ok)
        self.log_dir.mkdir(parents=True, exist_ok=exist_ok)

        # save updated config file to the checkpoint dir
        write_json(self.config, self.save_dir / "config.json")

    @classmethod
    def from_args(cls, args):
        if not isinstance(args, tuple):
            args = args.parse_args()

        cfg_fname = Path(args.config)
        config = read_json(cfg_fname)

        return cls(config)

    def init_obj(self, obj_dict, module, *args, **kwargs):
        module_name = obj_dict["type"]
        module_args = dict(obj_dict["args"])
        assert all(
            [k not in module_args for k in kwargs]
        ), "Overwriting kwargs given in config file is not allowed"
        module_args.update(kwargs)
        return getattr(module, module_name)(*args, **module_args)

    def __getitem__(self, name):
        return self.config[name]

    # setting read-only attributes
    @property
    def config(self):
        return self._config

    @property
    def save_dir(self):
        return self._save_dir

    @property
    def log_dir(self):
        return self._log_dir

    @classmethod
    def get_default_configs(cls):
        config_path = ROOT_PATH / "src" / "config.json"
        with config_path.open() as f:
            return cls(json.load(f))


def _update_config(config, modification):
    if modification is None:
        return config

    for k, v in modification.items():
        if v is not None:
            _set_by_path(config, k, v)
    return config


def _get_opt_name(flags):
    for flg in flags:
        if flg.startswith("--"):
            return flg.replace("--", "")
    return flags[0].replace("--", "")


def _set_by_path(tree, keys, value):
    keys = keys.split(";")
    _get_by_path(tree, keys[:-1])[keys[-1]] = value


def _get_by_path(tree, keys):
    return reduce(getitem, keys, tree)
