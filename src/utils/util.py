import json
from collections import OrderedDict
from itertools import repeat
from pathlib import Path
import shutil
import subprocess

import torch
import torch.nn as nn
import torchvision.transforms.functional as F


ROOT_PATH = Path(__file__).absolute().resolve().parent.parent.parent


def ensure_dir(dirname):
    dirname = Path(dirname)
    if not dirname.is_dir():
        dirname.mkdir(parents=True, exist_ok=False)


def read_json(fname):
    fname = Path(fname)
    with fname.open("rt") as handle:
        return json.load(handle, object_hook=OrderedDict)


def write_json(content, fname):
    fname = Path(fname)
    with fname.open("wt") as handle:
        json.dump(content, handle, indent=4, sort_keys=False)


def inf_loop(data_loader):
    """wrapper function for endless data loader."""
    for loader in repeat(data_loader):
        yield from loader


def set_dropouts_to_train(model):
    for child_name, child in model.named_children():
        if isinstance(child, nn.Dropout):
            child.train()
        else:
            set_dropouts_to_train(child)


@torch.no_grad()
def compute_fid(gen_model, dataloader, tmp_dir, device, n_images=1_000):
    fake_dir = Path(tmp_dir) / "fake"
    ensure_dir(fake_dir)
    real_dir = Path(tmp_dir) / "real"
    ensure_dir(real_dir)

    std = torch.tensor([0.5, 0.5, 0.5])
    mean = torch.tensor([0.5, 0.5, 0.5])

    cur_image_idx = 0
    gen_model.eval()
    for batch in inf_loop(dataloader):
        if cur_image_idx >= n_images:
            break

        for key in ["src_imgs"]:
            batch[key] = batch[key].to(device)

        fake_imgs = gen_model(**batch)
        fake_imgs = fake_imgs.cpu()

        for fake, real in zip(fake_imgs, batch["trg_imgs"]):
            if cur_image_idx >= n_images:
                break

            fake = fake * std[:, None, None] + mean[:, None, None]
            img = F.to_pil_image(fake)
            img.save(fake_dir / f"{cur_image_idx}.png")

            real = real * std[:, None, None] + mean[:, None, None]
            img = F.to_pil_image(real)
            img.save(real_dir / f"{cur_image_idx}.png")
            cur_image_idx += 1

    output = subprocess.run(
        ["python3", "-m", "pytorch_fid", fake_dir.resolve(), real_dir.resolve(), "--device", str(device)],
        capture_output=True
    )

    shutil.rmtree(fake_dir)
    shutil.rmtree(real_dir)

    if output.returncode:
        print("Error in pytorch-fid")
        print(output.stdout)
        print(output.stderr)
        return float("nan")

    output = output.stdout.decode("utf-8").strip().split("\n")[-1]
    return float(output[5:])
