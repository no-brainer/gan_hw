import os

import torch
import torch.nn as nn
import torch.nn.functional as F

from src.utils import MetricTracker, inf_loop, ensure_dir, compute_fid


class Trainer:

    def __init__(
            self,
            gen,
            crit_g,
            opt_g,
            disc,
            crit_d,
            opt_d,
            device,
            train_dataloader,
            val_dataloader,
            writer,
            **kwargs
    ):
        self.gen = gen
        self.crit_g = crit_g
        self.opt_g = opt_g

        self.disc = disc
        self.crit_d = crit_d
        self.opt_d = opt_d

        self.cur_epoch = kwargs.get("start_epoch", 1)
        self.num_epochs = kwargs.get("num_epochs", 100)

        self.len_epoch = kwargs.get("len_epoch", None)
        self.train_dataloader = train_dataloader
        if self.len_epoch is None:
            self.len_epoch = len(self.train_dataloader)

        self.val_dataloader = val_dataloader

        self.max_gradient_norm = kwargs.get("max_gradient_norm", 10.0)

        self.disc_update_step = kwargs.get("disc_update_step", 2)

        self.device = device

        self.save_step = kwargs.get("save_step", 5)
        self.save_dir = kwargs.get("save_dir", "./saved")
        ensure_dir(self.save_dir)

        self.tmp_dir = kwargs.get("tmp_dir", "./tmp")
        ensure_dir(self.tmp_dir)

        self.log_step = kwargs.get("log_step", 10)
        self.writer = writer

        self.train_metrics = MetricTracker(["loss_g", "loss_d", "grad_norm_g", "grad_norm_d"])
        self.test_metrics = MetricTracker(["loss_g", "loss_d"])

    @staticmethod
    def move_batch_to_device(batch, device):
        for key in ["src_imgs", "trg_imgs"]:
            batch[key] = batch[key].to(device)
        return batch

    def train(self):
        try:
            self._train()
        except KeyboardInterrupt as e:
            print("Interrupted by user. Saving checkpoint...")
            self._save_checkpoint()
            raise e

    def _train(self):
        start_epoch = self.cur_epoch
        for epoch in range(start_epoch, self.num_epochs + 1):
            self._train_epoch(epoch)

            if epoch % self.save_step == 0:
                self._save_checkpoint()

            self.cur_epoch += 1

    def process_batch(self, batch_idx, batch, metrics, is_training):
        batch = self.move_batch_to_device(batch, self.device)

        batch["pred_imgs"] = self.gen(**batch)
        batch["fake_probs"] = self.disc(batch["pred_imgs"], batch["src_imgs"])
        batch["real_probs"] = self.disc(batch["trg_imgs"], batch["src_imgs"])
        batch["loss_g"] = self.crit_g(**batch)

        batch["fake_probs"] = self.disc(batch["pred_imgs"].detach(), batch["src_imgs"])
        batch["loss_d"] = self.crit_d(**batch)
        if is_training:
            if batch_idx % self.disc_update_step:
                batch["loss_g"].backward()
                nn.utils.clip_grad_norm_(self.gen.parameters(), self.max_gradient_norm)
                self.opt_g.step()
                self.train_metrics.update("grad_norm_g", self.get_grad_norm(self.gen))
            else:
                batch["loss_d"].backward()
                self.opt_d.step()
                self.train_metrics.update("grad_norm_d", self.get_grad_norm(self.disc))

        metrics.update("loss_g", batch["loss_g"].item())
        metrics.update("loss_d", batch["loss_d"].item())

        return batch

    def _train_epoch(self, epoch):
        self.gen.train()
        self.disc.train()

        for batch_idx, batch in enumerate(inf_loop(self.train_dataloader)):
            if batch_idx >= self.len_epoch:
                break

            self.opt_g.zero_grad()
            self.opt_d.zero_grad()

            batch = self.process_batch(batch_idx, batch, self.train_metrics, is_training=True)

            if batch_idx % self.log_step == 0:
                self.writer.set_step((epoch - 1) * self.len_epoch + batch_idx, "train")
                for metric_name, value in self.train_metrics.all_avg().items():
                    self.writer.add_scalar(metric_name, value)

                self._log_media(**batch)
                self._log_distributions(**batch)
                self.train_metrics.reset()

        self._valid_epoch(epoch)

    def _valid_epoch(self, epoch):
        self.gen.eval()
        self.disc.eval()

        fake_probs = []
        real_probs = []
        with torch.no_grad():
            for batch_idx, batch in enumerate(self.val_dataloader):
                batch = self.process_batch(batch_idx, batch, self.test_metrics, is_training=False)
                fake_probs.append(batch["fake_probs"])
                real_probs.append(batch["real_probs"])

            self.writer.set_step(epoch * self.len_epoch, "val")

            for metric_name, value in self.test_metrics.all_avg().items():
                self.writer.add_scalar(metric_name, value)

            fake_probs = torch.vstack(fake_probs)
            real_probs = torch.vstack(real_probs)

            self._log_media(**batch)
            self._log_distributions(fake_probs, real_probs)
            self.test_metrics.reset()

            fid_score = compute_fid(self.gen, self.val_dataloader, self.tmp_dir, self.device)
            self.writer.add_scalar("fid", fid_score)

    def _log_media(self, pred_imgs, src_imgs, trg_imgs, num_examples=5, *args, **kwargs):
        self.writer.add_image("generated", pred_imgs[:num_examples].detach().cpu(), no_norm=False)
        self.writer.add_image("source", src_imgs[:num_examples].detach().cpu(), no_norm=False)
        self.writer.add_image("target", trg_imgs[:num_examples].detach().cpu(), no_norm=False)

    def _log_distributions(self, fake_probs, real_probs, *args, **kwargs):
        self.writer.add_histogram("fake_probs", fake_probs.detach().cpu().flatten().tolist())
        self.writer.add_histogram("real_probs", real_probs.detach().cpu().flatten().tolist())

    def _save_checkpoint(self):
        state = {
            "epoch": self.cur_epoch,
            "g_state_dict": self.gen.state_dict(),
            "g_optimizer": self.opt_g.state_dict(),
            "d_state_dict": self.disc.state_dict(),
            "d_optimizer": self.opt_d.state_dict(),
        }

        filename = os.path.join(self.save_dir, f"checkpoint_epoch{self.cur_epoch}.pth")
        print(f"Saving checkpoint to {filename}")
        torch.save(state, filename)

    @torch.no_grad()
    def get_grad_norm(self, model):
        params = [p for p in model.parameters() if p.grad is not None]
        total_norm = torch.norm(torch.stack([torch.norm(p.grad.detach(), p=2).cpu() for p in params]), p=2)
        return total_norm.item()
