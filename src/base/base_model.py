import torch.nn as nn


class BaseModel(nn.Module):

    def __init__(self, *args, **kwargs):
        super(BaseModel, self).__init__()

    def forward(self, *args, **kwargs):
        raise NotImplemented()

    def __str__(self):
        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        params = sum([p.numel() for p in model_parameters])
        return super().__str__() + "\nTrainable parameters: {}".format(params)
