import torch
import torch.nn as nn

from src.base.base_model import BaseModel
from src.models.modules import DownBlock


class UNetDiscriminator(BaseModel):

    def __init__(
            self,
            mode,
            down_filters,
            down_activation,
            down_dropouts
    ):
        super(UNetDiscriminator, self).__init__()

        self.is_conditional = mode == "conditional"
        in_filters_start = 6 if self.is_conditional else 3

        layers = []
        for i in range(len(down_filters)):
            in_filters = in_filters_start if i == 0 else down_filters[i - 1]
            layers.append(
                DownBlock(in_filters, down_filters[i], down_activation, dropout=down_dropouts[i], add_norm=i > 0)
            )

        layers.extend([
            nn.Conv2d(down_filters[-1], 1, 1),
            nn.Sigmoid(),
        ])
        self.net = nn.Sequential(*layers)

    def forward(self, imgs, src_imgs):
        if self.is_conditional:
            imgs = torch.cat([src_imgs, imgs], dim=1)

        return self.net(imgs)
