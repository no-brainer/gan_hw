import torch
import torch.nn as nn


class ResizeConv(nn.Module):
    """
    According to https://distill.pub/2016/deconv-checkerboard/
    this layer should replace ConvTransposed to rid of checkered artifacts
    """
    def __init__(self, in_filters, out_filters):
        super(ResizeConv, self).__init__()

        self.net = nn.Sequential(
            nn.Upsample(scale_factor=2, mode="nearest"),
            nn.ReflectionPad2d(1),
            nn.Conv2d(in_filters, out_filters, kernel_size=3, stride=1, padding=0),
        )

    def forward(self, x):
        return self.net(x)


class DownBlock(nn.Module):

    def __init__(
            self,
            in_filters,
            out_filters,
            activation_layer,
            kernel_size=4,
            stride=2,
            padding=1,
            dropout=0.0,
            add_norm=True
    ):
        super(DownBlock, self).__init__()

        layers = [
            nn.Conv2d(in_filters, out_filters, kernel_size,
                      stride=stride, padding=padding, bias=(not add_norm))
        ]
        if add_norm:
            layers.append(nn.InstanceNorm2d(out_filters))

        layers.extend([
            activation_layer,
            nn.Dropout(dropout)
        ])

        self.net = nn.Sequential(*layers)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.net(x)


class UpBlock(nn.Module):

    def __init__(
            self,
            in_filters,
            out_filters,
            activation_layer,
            kernel_size=4,
            stride=2,
            padding=1,
            dropout=0.0,
            add_norm=True
    ):
        super(UpBlock, self).__init__()

        layers = [
            nn.ConvTranspose2d(in_filters, out_filters, kernel_size,
                               stride=stride, padding=padding, bias=(not add_norm)),
        ]
        if add_norm:
            layers.append(nn.InstanceNorm2d(out_filters))

        layers.extend([
            activation_layer,
            nn.Dropout(dropout)
        ])

        self.net = nn.Sequential(*layers)

    def forward(self, x: torch.Tensor, skip_input: torch.Tensor) -> torch.Tensor:
        return torch.cat((self.net(x), skip_input), dim=1)

