import torch
import torch.nn as nn

from src.base.base_model import BaseModel
from src.models.modules import UpBlock, DownBlock
from src.utils import set_dropouts_to_train


class UNetGenerator(BaseModel):

    def __init__(
            self,
            down_filters,
            down_activation,
            down_dropouts,
            up_filters,
            up_activation,
            up_dropouts,
    ):
        super(UNetGenerator, self).__init__()

        self.down_modules = nn.ModuleList()
        for i in range(len(down_filters)):
            in_filters = 3 if i == 0 else down_filters[i - 1]
            add_norm = 0 < i and i + 1 < len(down_filters)
            self.down_modules.append(
                DownBlock(in_filters, down_filters[i], down_activation, dropout=down_dropouts[i], add_norm=add_norm)
            )

        self.up_modules = nn.ModuleList()
        for i in range(len(up_filters)):
            in_filters = down_filters[-1] if i == 0 else up_filters[i - 1] + down_filters[-i - 1]
            self.up_modules.append(
                UpBlock(in_filters, up_filters[i], up_activation, dropout=up_dropouts[i])
            )

        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(up_filters[-1] + down_filters[0], 3, 4, stride=2, padding=1),
            nn.Tanh()
        )

    def forward(self, src_imgs: torch.Tensor, *args, **kwargs) -> torch.Tensor:
        x = src_imgs
        down_outputs = []
        for layer in self.down_modules:
            x = layer(x)
            down_outputs.append(torch.clone(x))

        down_outputs.pop()

        for layer in self.up_modules:
            x = layer(x, down_outputs[-1])
            down_outputs.pop()

        return self.decoder(x)

    def eval(self):
        super(UNetGenerator, self).eval()
        set_dropouts_to_train(self)
