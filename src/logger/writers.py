from timeit import default_timer as timer


import torchvision.transforms.functional as F


class BaseWriter:

    def __init__(self, proj_name, config):
        self.proj_name = proj_name
        self.config = config

        self.step = None
        self.mode = None
        self.cur_time = None

    def __enter__(self):
        self.step = 0
        self.mode = None
        self.cur_time = timer()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def get_logging_name(self, name):
        return "_".join((name, self.mode))

    def set_step(self, step, mode):
        step_skip = step - self.step
        self.step = step
        self.mode = mode
        if step > 0:
            self.add_scalar("steps_per_sec", step_skip / (timer() - self.cur_time))

        self.cur_time = timer()

    def add_scalar(self, name, value):
        raise NotImplementedError()

    def add_image(self, name, imgs, no_norm=True):
        raise NotImplementedError()

    def add_histogram(self, name, values):
        raise NotImplemented()


class WBWriter(BaseWriter):

    def __init__(self, proj_name, config):
        super(WBWriter, self).__init__(proj_name, config)

        try:
            import wandb

            self.wandb = wandb
        except ImportError:
            raise RuntimeError("Wandb is not available, use another writer")

        self.wandb.login()
        self.cur_run = None

    def __enter__(self):
        self.cur_run = self.wandb.init(
            project=self.proj_name, config=self.config, reinit=True
        )

        return super(WBWriter, self).__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur_run.finish()
        self.cur_run = None

    def add_scalar(self, name, value):
        self.wandb.log({self.get_logging_name(name): value}, step=self.step)

    def add_image(self, name, imgs, no_norm=True):
        if no_norm:
            imgs = [F.to_pil_image(img) for img in imgs]

        self.wandb.log({
            self.get_logging_name(name): [self.wandb.Image(img) for img in imgs]
        }, step=self.step)

    def add_histogram(self, name, values):
        self.wandb.log({
            self.get_logging_name(name): self.wandb.Histogram(values)
        }, step=self.step)


class PlaceholderWriter(BaseWriter):

    def add_scalar(self, name, value):
        pass

    def add_image(self, name, imgs, no_norm=True):
        pass

    def add_histogram(self, name, values):
        pass
